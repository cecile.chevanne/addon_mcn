import bpy



class MCN_OT_CC(bpy.types.Operator):
    """"""
    bl_idname = "object.monkey_generator"
    bl_label = "Monkey generator"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.mesh.primitive_monkey_add()
        return {'FINISHED'}
    


class MCN_PT_CC(bpy.types.Panel):
    """"""
    bl_label = "CC"
    bl_idname = "MCN_PT_CC"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}
    
    

    def draw(self, context):
        layout = self.layout

        layout.label(text="Générateur de Suzanne")
        
        col = layout.column()
        col.operator("mesh.primitive_monkey_add")



def register():
    bpy.utils.register_class(MCN_PT_CC)
    bpy.utils.register_class(MCN_OT_CC)

def unregister():
    bpy.utils.unregister_class(MCN_PT_CC)
    bpy.utils.unregister_class(MCN_OT_CC)